<p>Application Android (bêta) qui permet de geler complètement toutes les activités en tâche de fond de n'importe quelle application.</p>
<ul>
<li>Reprenez le contrôle de tout ce qui fonctionne sur votre téléphone<li>
<li>Améliorez l'autonomie et réduisez l'usage des données mobile en gelant les applications rarement utilisées</li>
<li>Particulièrement utile lors d'un déplacement où vous avez besoin de seulement quelques applications et d'une bonne autonomie</li>
</ul>
<p>Greenify peut également le faire mais son code n'est pas ouvert.</p>
<p>SuperFreezZ n'est pas un gestionnaire de tâches qui vous promet de supprimer 10 Go de données par mois ou de rendre votre appareil 2× plus rapide. C'est impossible.</p>
<p>À la place, SuperFreezZ est honnête sur ses désavantages : geler vos applications quotidiennes va certainement réduire légèrement votre autonomie. Ces applications vont également mettre plus de temps à démarrer la prochaine fois que vous les lancerez : SuperFreezZ va « surgeler » vos applications et il faut environ 1-3 secondes pour les dégeler. Greenify a les même désavantages, cependant l'auteur de Greenify ne vous en prévient pas. Aussi : ne cherchez pas à en faire trop et SuperFreezZ sera super utile.</p>
<p>Exemples d'applications qui méritent d'être gelées :</p>
<ul>
<li>Les applications dans lesquelles vous n'avez pas confiance (que vous ne voulez pas laisser fonctionner en arrière plan)</li>
<li>Les applications que vous n'utilisez pas souvent</li>
<li>Les applications irritantes</li>
</ul>
<h2 id="features">Fonctionnalités</h2>
<ul>
<li>Peut fonctionner optionnellement sans service d'accessibilité, ce qui ralentit l'appareil</li>
<li>Peut geler uniquement les applications inutilisées depuis une semaine (peut être configuré)</li>
<li>Choix par liste blanche (tout est gelé par défaut) ou par liste noire (aucun gel par défaut)</li>
<li>Peut geler les applications lors de l'extinction de l'écran</li>
<li>Options pour geler les applications systèmes et SuperFreezZ lui-même</li>
<li>Logiciel libre complètement open source</li>
</ul>
<h2 id="contributing-to-superfreezz">Participer à SuperFreezZ</h2>
<h3 id="development">Développement</h3>
<p>Si vous avez un problème, une question ou une idée, ouvrez simplement un ticket !</p>
<p>Si vous souhaitez aider au développement ou avez une idée de quelque chose à améliorer, jetez un œil aux tickets ouverts ou ouvrez en un nouveau.</p>
<p>Dites-moi ce que vous allez faire pour éviter d'implémenter la même chose en même temps :-)</p>
<h3 id="translate">Traduction</h3>
<p>Vous pouvez <a href="https://hosted.weblate.org/engage/superfreezz/">traduire SuperFreezZ sur Weblate</a>.</p>
<h2 id="credits">Crédits</h2>
<p>Le code pour afficher la liste des applications provient de <a href="https ://f-droid.org/wiki/page/axp.tool.apkextractor">ApkExtractor</a>.</p>
<p>Robin Naumann a fait une introduction sympa. Elle a été créée avec la librairie AppIntro.</p>
<p>Les images du design proviennent de : https ://pixabay.com/photos/thunder-lighting-lightning-cloud-1368797/, le texte a été ajouté avec https ://www.norio.be/android-feature-graphic-generator/.</p>
<h2 id="qa">Questions/Réponses</h2>
<p>Q/R :</p>
<p>Q : Quelle est la différence entre l'hibernation et le gel d'une application ? R : Il n'y en a aucune. Si vous hibernez une application dans Greenify elle sera montrée comme gelée dans SuperFreezZ ou toute autre méthode.</p>
<p>Q : Mais l'orthographe correcte serait « SuperFreeze » ! R : Je sais.</p>
<p>Q : Est-il prévu de rendre SuperFreezZ payant ? R : Non.</p>
