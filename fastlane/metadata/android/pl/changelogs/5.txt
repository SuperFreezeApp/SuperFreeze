Ustabilizowano wyszukiwanie przycisku Force Stop, problem #20 ?
Próba ponownego ustabilizowania zamrażania. Usunięto nieużywaną funkcję doOnFinished() z FreezerService.
Poprawka regresji: nie używaj limitu czasu, gdy usługa freezerservice nie jest uruchomiona
