Bugfix: Sprawiono, aby SF działał ze starszymi wersjami Androida
Ręczne zamrażanie: powiedz użytkownikowi, aby nacisnął „Wymuś zatrzymanie”, „OK” i „Wstecz”, poprawka #14
Dodano zrzuty ekranu
Tłumaczenia
Nie zamrażaj automatycznie samego SuperFreezZ
Traktuj aplikację jako „używaną” tylko wtedy, gdy była używana przez co najmniej 2 sekundy
Naprawiono ustalenie, czy aplikacja była „ostatnio używana”
