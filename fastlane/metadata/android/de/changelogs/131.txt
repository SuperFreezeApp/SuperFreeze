Absturz behoben, allgemeine Fehlerbehebungen
Erzwingen des Einfrierens besser positioniert, könnte #96 beheben
Dunklen Design in Android 10 unterstützt, wenn der "Energiesparmodus" aktiviert wird
Den Nutzenden wird deutlicher geraten, die Funktion "Ein/Aus sperrt Gerät sofort" zu deaktivieren
Zu häufige Anzeige von '(inaktiv)' behoben
'(inaktiv)' statt '(jetzt inaktiv)' eingesetzt, da niemand versteht, was dieses 'jetzt' bedeuten soll
Neues symmetrischeres Symbol
Übersetzungen aktualisiert
