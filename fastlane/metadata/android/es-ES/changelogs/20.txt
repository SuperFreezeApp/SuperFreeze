Agrupación agregada por aplicaciones de usuario/sistema, comportamiento de congelación estándar configurable agregado para aplicaciones del sistema. ¡Gracias Dark Immortal!
En el sistema Install 10, se solucionó la función de congelación automática. ¡Gracias de nuevo Darkimmortal!
Se corrigió la clasificación ascendente/descendente en el n.° 67
Ignorar si no hay respuesta java.lang.IndexOutOfBoundsException
Se solucionó el problema en el n. ° 61 donde al hacer clic en "Nunca congelar", el elemento debería aparecer.
Información de versión fija en "Configuración - Acerca de" en #63
Se corrigió el error en el n. ° 62 donde hacer clic en una aplicación solo debería congelar esa aplicación
Soporte de teclado básico de Fire TV
actualización de traducción
